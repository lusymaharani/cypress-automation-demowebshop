import RegisterPage from '../support/pageObject/registerPage';
import LoginPage from '../support/pageObject/loginPage';

describe("Registrasi dan Login", () => {
  beforeEach(() => {
    cy.fixture("user").then((userData) => {
      cy.visit("/");
      RegisterPage.visit();
      RegisterPage.fillRegistrationForm(
        "Lusy",
        "Hellena",
        userData.email,
        userData.password
      );
      RegisterPage.submitForm();
      LoginPage.visit();
    });
  });

  it("Gagal melakukan login dengan data yang telah terdaftar", () => {
    cy.fixture("user").then((userData) => {
      LoginPage.fillLoginForm(userData.email, userData.password);
      LoginPage.submitForm();
    });
  });

  it("Gagal melakukan login dengan password yang telah salah", () => {
    cy.fixture("user").then((userData) => {
      LoginPage.fillLoginForm(userData.email, userData.wrongPassword);
      LoginPage.submitForm();
      cy.contains("Login was unsuccessful. Please correct the errors and try again. The credentials provided are incorrect").should("exist");
    });
  });

  it("Gagal melakukan login dengan email yang telah belum terdaftar", () => {
    cy.fixture("user").then((userData) => {
      LoginPage.fillLoginForm(userData.wrongEmail, userData.wrongPassword);
      LoginPage.submitForm();
      cy.contains("Login was unsuccessful. Please correct the errors and try again. No customer account found").should("exist");
    });
  });

  it("Berhasil muncul pesan kesalahan ketika mencoba mendaftar dengan email yang sama", () => {
    cy.fixture("user").then((userData) => {
      cy.visit("/");
      RegisterPage.visit();
      RegisterPage.fillRegistrationForm(
        "Lucy",
        "Geneva",
        userData.email,
        userData.password
      );
      RegisterPage.submitForm();
      cy.contains("The specified email already exists").should("exist");
    });
  });
});
