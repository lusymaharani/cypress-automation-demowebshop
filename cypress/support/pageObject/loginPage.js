class LoginPage {
    visit() {
      cy.visit("/login");
    }
  
    fillLoginForm(email, password) {
      cy.get("#Email").type(email);
      cy.get("#Password").type(password);
    }
  
    submitForm() {
      cy.get('form > .buttons > .button-1').click();
    }
  }
  
  export default new LoginPage();
  