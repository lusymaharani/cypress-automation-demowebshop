class RegisterPage {
    visit() {
      cy.visit("/register");
    }
  
    fillRegistrationForm(firstName, lastName, email, password) {
      cy.get("#FirstName").type(firstName);
      cy.get("#LastName").type(lastName);
      cy.get("#Email").type(email);
      cy.get("#Password").type(password);
      cy.get("#ConfirmPassword").type(password);
    }
  
    submitForm() {
      cy.get('#register-button').click();
    }
  }
  
  export default new RegisterPage();
  