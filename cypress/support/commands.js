Cypress.Commands.add("register", (email, password) => {
    cy.visit("/register");
    cy.get("#FirstName").type("John");
    cy.get("#LastName").type("Doe");
    cy.get("#Email").type(email);
    cy.get("#Password").type(password);
    cy.get("#ConfirmPassword").type(password);
    cy.get('input[type="submit"]').click();
  });
  
  Cypress.Commands.add("login", (email, password) => {
    cy.visit("/login");
    cy.get("#Email").type(email);
    cy.get("#Password").type(password);
    cy.get('input[type="submit"]').click();
  });
  